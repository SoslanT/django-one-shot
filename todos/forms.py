from django.forms import ModelForm
from todos.models import TodoItem, TodoList
from django import forms


class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = "__all__"


class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
